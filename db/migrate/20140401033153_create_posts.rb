class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :body

      t.timestamps
    end

    create_table :comments do |t|
    	t.belongs_to :post
    	t.integer :post_id
    	t.text :body
    end 

  end
end
